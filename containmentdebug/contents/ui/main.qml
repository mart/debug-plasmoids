// -*- coding: iso-8859-1 -*-
/*
    SPDX-FileCopyrightText: 2021 Marco Martin <mart@kde.org>

    SPDX-License-Identifier: LGPL-2.0-or-later
*/

import QtQuick 2.0
import QtQuick.Layouts 1.4
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.components 3.0 as PlasmaComponents
import org.kde.plasma.plasmoid 2.0

ColumnLayout {
    id: root
    property int availableScreenRectChanges: 0

    implicitWidth: PlasmaCore.Units.gridUnit * 15
    property var screenRects: []
    property var availableScreenRects: []
    Plasmoid.onAvailableScreenRectChanged: {
        availableScreenRects.push(plasmoid.availableScreenRect.toString())
        availableScreenRectsChanged();
    }
    Plasmoid.onScreenGeometryChanged: {
        screenRects.push(plasmoid.screenGeometry.toString())
        screenRectsChanged();
    }

    Component.onCompleted: {
        availableScreenRects.push(plasmoid.availableScreenRect.toString())
        screenRects.push(plasmoid.screenGeometry.toString())
    }

    PlasmaComponents.Label {
        text: i18n("Screen Geometry changed %1 times:", root.screenRects.length);
    }
    Flow {
        Layout.fillWidth: true
        Repeater {
            model: root.screenRects.length
            PlasmaComponents.Label {
                text: root.screenRects[modelData]
            }
        }
    }



    PlasmaComponents.Label {
        text: i18n("AvailableScreenRect changes %1 times:", root.availableScreenRects.length);
    }
    Flow {
        Layout.fillWidth: true
        Repeater {
            model: root.availableScreenRects.length
            PlasmaComponents.Label {
                text: root.availableScreenRects[modelData]
            }
        }
    }
}
